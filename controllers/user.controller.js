const User = require('../models/user.model');

//Simple version, without validation or sanitation
exports.test = function (req, res) {
    res.send('Greetings from the Test controller!');
};

exports.user_create = function (req, res,next) {
    let user = new User(
        {
            name: req.body.name,
            age: req.body.age,
            gender:req.body.gender,
            address:req.body.address
        }
    );

    user.save(function (err) {
        if (err) {
            return next(err);
        }
        res.send('UserInfo Created successfully')
    });
};

exports.user_details = function (req, res,next) {
    User.findById(req.params.id, function (err, user) {
        if (err) return next(err);
        res.send(user);
    })
};

exports.user_update = function (req, res,next) {
    User.findByIdAndUpdate(req.params.id, {$set: req.body}, function (err, user) {
        if (err) return next(err);
        res.send('User updated.');
    });
};

exports.user_delete = function (req, res,next) {
    User.findByIdAndRemove(req.params.id, function (err) {
        if (err) return next(err);
        res.send('Deleted successfully!');
    })
};
exports.user_all = function(req,res,next){
    User.find({}, function(err, users) {
    var userMap = {};
      users.forEach(function(user) {
      userMap[user._id] = user;
    });
    res.send(userMap);  
  });
};